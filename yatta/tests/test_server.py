import os
import tempfile
from datetime import datetime

import pytest

from yatta import server

NOT_FOUND = b'{\n  "error": "404 Not Found: The requested URL was not found on the server. If you entered the URL manually please check your spelling and try again."\n}\n'
TODO_URL = "/api/v1/todo/"
TEST_DATE = datetime.strptime("2020-04-01 12:00:00", "%Y-%m-%d %H:%M:%S")


def populate_db():
    now_date = datetime.now()
    Test_Tasks = [
        server.Tasks(
            name="New Task 1",
            location="Home",
            notes="Some notes",
            created_at=TEST_DATE,
            updated_at=TEST_DATE,
        ),
        server.Tasks(
            name="Task 2",
            location="Work",
            notes=None,
            created_at=TEST_DATE,
            updated_at=TEST_DATE,
        ),
    ]
    server.DB.session.add_all(Test_Tasks)
    server.DB.session.commit()


@pytest.fixture
def client():
    db_fd, file_name = tempfile.mkstemp()
    server.APP.config["TESTING"] = True
    server.APP.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + file_name

    with server.APP.test_client() as client:
        with server.APP.app_context():
            server.DB.create_all()
            populate_db()
        yield client

    os.close(db_fd)
    os.unlink(file_name)


# TODO: mock better date
def test_index(client):
    rv = client.get("/", json={})
    json_data = rv.get_json()
    assert json_data["info"] == "use /api/v1/todo/"


def test_task_id(client):
    rv = client.get(TODO_URL + "1")
    json_data = rv.get_json()
    assert json_data['created_at'] == "2020-04-01T12:00:00"
    assert json_data['finished_at'] is None
    assert json_data['id'] == 1
    assert json_data['location'] == "Home"
    assert json_data['name'] == "New Task 1"
    assert json_data['notes'] == "Some notes"
    assert json_data['updated_at'] == "2020-04-01T12:00:00"
    assert rv.status_code == 200


def test_non_existing_task_id(client):
    rv = client.get(TODO_URL + "500")
    assert NOT_FOUND in rv.data
    assert rv.status_code == 404


def test_delete_task(client):
    rv = client.delete(TODO_URL + "2")
    assert b"" in rv.data
    assert rv.status_code == 204
    rv = client.get(TODO_URL + "2")
    assert NOT_FOUND in rv.data
    assert rv.status_code == 404


def test_update_task(client):
    rv = client.patch(TODO_URL + "2", json={"name": "another one"})
    json_data = rv.get_json()
    assert json_data['created_at'] == "2020-04-01T12:00:00"
    assert json_data['finished_at'] is None
    assert json_data['id'] == 2
    assert json_data['location'] == "Work"
    assert json_data['name'] == "another one"
    assert json_data['notes'] is None
    assert rv.status_code == 201


def test_tasks(client):
    rv = client.get(TODO_URL)
    json_data = rv.get_json()
    assert json_data[0]['created_at'] == "2020-04-01T12:00:00"
    assert json_data[0]['finished_at'] is None
    assert json_data[0]['id'] == 1
    assert json_data[0]['location'] == "Home"
    assert json_data[0]['name'] == "New Task 1"
    assert json_data[0]['notes'] == "Some notes"
    assert json_data[1]['created_at'] == "2020-04-01T12:00:00"
    assert json_data[1]['finished_at'] is None
    assert json_data[1]['id'] == 2
    assert json_data[1]['location'] == "Work"
    assert json_data[1]['name'] == "Task 2"
    assert json_data[1]['notes'] is None
    assert rv.status_code == 200


def test_create_task(client):
    rv = client.post(
        TODO_URL,
        json={
            "name": "something new",
            "location": "Work",
            "notes": "Hello World",
        },
    )
    json_data = rv.get_json()
    assert json_data['name'] == "something new"
    assert json_data['location'] == "Work"
    assert json_data['notes'] == "Hello World"
    assert json_data['created_at'] is not None
    assert rv.status_code == 201
