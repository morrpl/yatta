import os
import pathlib
import flask
import yaml
from datetime import datetime
from flask import request, jsonify
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy


APP = flask.Flask(__name__)
APP.config["DEBUG"] = True


CONFIG_FILE = os.getenv("YATTA_CONFIG", default="config.yaml")


def load_config(CONFIG_FILE):
    with open("config.yaml", "r") as file_data:
        yaml_data = file_data.read()

    try:
        return yaml.safe_load(yaml_data)
    except yaml.YAMLError as e:
        print(e)


config = load_config(CONFIG_FILE)

APP.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
APP.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
    pathlib.Path().absolute(), config.get("db_file")
)
DB = SQLAlchemy(APP)
MA = Marshmallow(APP)


@APP.errorhandler(404)
def resource_not_found(e):
    return jsonify(error=str(e)), 404


# DB schemas
class Tasks(DB.Model):
    id = DB.Column(DB.Integer, primary_key=True)
    name = DB.Column(DB.TEXT)
    location = DB.Column(DB.TEXT)
    created_at = DB.Column(DB.TIMESTAMP)
    updated_at = DB.Column(DB.TIMESTAMP)
    finished_at = DB.Column(DB.TIMESTAMP)
    notes = DB.Column(DB.TEXT)


# Marshmallow part
class TaskSchema(MA.SQLAlchemyAutoSchema):
    class Meta:
        model = Tasks


@APP.route("/", methods=["GET"])
def home():
    return jsonify({"info": "use /api/v1/todo/"})


@APP.route("/api/v1/todo/", methods=["GET"])
def get_all_tasks():
    tasks = Tasks.query.all()
    return TASKS_SCHEMA.jsonify(tasks)


@APP.route("/api/v1/todo/", methods=["POST"])
def add_task():
    now_time = datetime.now()
    new_task = Tasks(
        name=request.json["name"],
        location=request.json["location"],
        notes=request.json["notes"],
        created_at=now_time,
        updated_at=now_time,
        finished_at=None,
    )
    DB.session.add(new_task)
    DB.session.commit()
    return TASK_SCHEMA.dump(new_task), 201


@APP.route("/api/v1/todo/<int:task_id>", methods=["GET"])
def get_single_task(task_id):
    task = Tasks.query.get_or_404(task_id)
    return TASK_SCHEMA.jsonify(task)


@APP.route("/api/v1/todo/<int:task_id>", methods=["DELETE"])
def delete_task(task_id):
    task = Tasks.query.get_or_404(task_id)
    DB.session.delete(task)
    DB.session.commit()
    return "", 204


@APP.route("/api/v1/todo/<int:task_id>", methods=["PATCH"])
def update_task(task_id):
    task = Tasks.query.get_or_404(task_id)
    if request.json.get("name"):
        task.name = request.json["name"]
    if request.json.get("location"):
        task.location = request.json["location"]
    if request.json.get("notes"):
        task.notes = request.json["notes"]
    task.updated_at = datetime.now()
    DB.session.commit()
    return TASK_SCHEMA.dump(task), 201


TASK_SCHEMA = TaskSchema()
TASKS_SCHEMA = TaskSchema(many=True)

if __name__ == "__main__":
    DB.create_all()
    APP.run()
