import setuptools

desc = "Yet Another ToDo/Task Application"
setuptools.setup(
    name="yatta",
    version="0.0.1",
    author="Daniel Horecki",
    author_email="morr@morr.pl",
    description=desc,
    long_description=desc,
    url="https://gitlab.com/morrpl/yatta",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
