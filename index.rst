.. yatta documentation master file

Welcome to yatta's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. include:: ./README.rst

.. openapi:: swagger.yml

